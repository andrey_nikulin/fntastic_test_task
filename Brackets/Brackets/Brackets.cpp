// Example program
#include <iostream>
#include <string>
#include <unordered_map>

constexpr char A_DIFF = 'a' - 'A';

void bracketification(std::string& str) {
    std::unordered_map<char, unsigned> ascii;
    for (unsigned i = 0; i < str.size(); ++i) {
        char symbol = str[i];

        //letter case control
        if (symbol >= 'A' && symbol <= 'Z') {
            symbol += A_DIFF;
        }

        unsigned index = ascii[symbol];
        if (index != 0) {
            str[i] = ')';
            str[index - unsigned(1)] = ')';
        }
        else {
            str[i] = '(';
            ascii[symbol] = i + 1;
        }
    }
}

int main(int argc, char** argv) {
    //original word
    std::string word = "";

    if (argc == 2) {
        word = argv[1];
    }
    else {
        std::cout << "enter a word, please: ";
        std::getline(std::cin, word);
    }

    bracketification(word);
    std::cout << word << std::endl;

    return 0;
}