// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseNest.h"
#include "BaseTurtlePawn.h"

#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABaseNest::ABaseNest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	NestMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("NestMesh"));
	NestMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	NestMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);

	RootComponent = NestMeshComponent;
}

// Called when the game starts or when spawned
void ABaseNest::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseNest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseNest::SpawnTurtle_Implementation() {
	if (BaseTurtlePawnClass) {
		const FVector ForwardLocation = GetActorForwardVector() * 75;
		const FVector SpawnLocation = GetActorLocation() + ForwardLocation;

		ABaseTurtlePawn* TurtlePawn = GetWorld()->SpawnActor<ABaseTurtlePawn>(BaseTurtlePawnClass,
			SpawnLocation,
			GetActorRotation());

		if (IsValid(TurtlePawn)) {
			TurtlePawn->DestinationTarget = DestinationTarget;
		}
	}
}

