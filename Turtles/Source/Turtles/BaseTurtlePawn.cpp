// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseTurtlePawn.h"

#include "Components/CapsuleComponent.h"
#include "Components/SkeletalMeshComponent.h"

#include "GameFramework/FloatingPawnMovement.h"

#include "Kismet/GameplayStatics.h"

// Sets default values
ABaseTurtlePawn::ABaseTurtlePawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CapsuleCollider = CreateDefaultSubobject<UCapsuleComponent>(TEXT("CapsuleCollider"));
	CapsuleCollider->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Block);
	CapsuleCollider->SetCapsuleHalfHeight(22);
	CapsuleCollider->SetCapsuleRadius(22);

	RootComponent = CapsuleCollider;

	SkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TurtleMesh"));
	SkeletalMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SkeletalMesh->SetCollisionResponseToAllChannels(ECR_Overlap);
	SkeletalMesh->SetWorldLocation(FVector(0.f, 0.f, 0.f));
	SkeletalMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);

	PawnMovement = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("TurtleMovement"));
	PawnMovement->MaxSpeed = 400;
	PawnMovement->NavAgentProps.bCanWalk = true;
}

void ABaseTurtlePawn::TryToPlaySound(USoundWave* sound) {
	if (IsValid(sound)) {
		UGameplayStatics::PlaySoundAtLocation(this, sound, GetActorLocation());
	}
}

// Called when the game starts or when spawned
void ABaseTurtlePawn::BeginPlay()
{
	Super::BeginPlay();
	TryToPlaySound(AppierSound);
}

// Called every frame
void ABaseTurtlePawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ABaseTurtlePawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ABaseTurtlePawn::Destroyed() {
	Super::Destroyed();

	TryToPlaySound(FinishSound);
}