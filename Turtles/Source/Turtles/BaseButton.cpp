// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseButton.h"
#include "BaseNest.h"

#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ABaseButton::ABaseButton()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	ButtonMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ButtonMesh"));
	ButtonMeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	ButtonMeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
	ButtonMeshComponent->SetWorldLocation(FVector(0.f, 0.f, 0.f));

	RootComponent = ButtonMeshComponent;
}

// Called when the game starts or when spawned
void ABaseButton::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABaseButton::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABaseButton::Press_Implementation() {
	if (IsValid(Nest)) {
		Nest->SpawnTurtle();
	}
}