// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"

#include "Math/Vector.h"

#include "BaseTurtlePawn.generated.h"

class USoundWave;
class UCapsuleComponent;
class USkeletalMeshComponent;
class UFloatingPawnMovement;

UCLASS()
class TURTLES_API ABaseTurtlePawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ABaseTurtlePawn();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UCapsuleComponent* CapsuleCollider;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	USkeletalMeshComponent* SkeletalMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UFloatingPawnMovement* PawnMovement;

	UPROPERTY(EditDefaultsOnly)
	USoundWave* AppierSound;

	UPROPERTY(EditDefaultsOnly)
	USoundWave* FinishSound;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite)
	FVector DestinationTarget;

private:

	void TryToPlaySound(USoundWave* sound);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void Destroyed() override;

};
