// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TurtlesHUD.generated.h"

UCLASS()
class TURTLES_API ATurtlesHUD : public AHUD
{
	GENERATED_BODY()

public:
	ATurtlesHUD();

	virtual void DrawHUD() override;

private:

	class UTexture2D* CrosshairTexture;
	
};
