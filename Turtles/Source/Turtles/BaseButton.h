// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseButton.generated.h"

class UStaticMeshComponent;
class ABaseNest;

UCLASS()
class TURTLES_API ABaseButton : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseButton();

	UPROPERTY(BlueprintReadWrite)
	UStaticMeshComponent* ButtonMeshComponent;

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
	ABaseNest* Nest;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void Press();
	void Press_Implementation();

};