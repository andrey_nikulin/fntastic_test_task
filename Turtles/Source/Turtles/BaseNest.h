// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "Math/Vector.h"

#include "BaseNest.generated.h"

class UStaticMeshComponent;
class ABaseTurtlePawn;

UCLASS()
class TURTLES_API ABaseNest : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABaseNest();

	UPROPERTY(BlueprintReadWrite)
	UStaticMeshComponent* NestMeshComponent;

	UPROPERTY(EditAnywhere)
	TSubclassOf<ABaseTurtlePawn> BaseTurtlePawnClass;

	UPROPERTY(EditInstanceOnly)
	FVector DestinationTarget;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SpawnTurtle();
	void SpawnTurtle_Implementation();

};
